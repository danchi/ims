﻿using Microsoft.EntityFrameworkCore;

namespace IMS.Persistence
{
    public class IMSDbContextFactory : DesignTimeDbContextFactoryBase<IMSDbContext>
    {
        protected override IMSDbContext CreateNewInstance(DbContextOptions<IMSDbContext> options)
        {
            return new IMSDbContext(options);
        }
    }
}