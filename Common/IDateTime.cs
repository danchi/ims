﻿using System;

namespace IMS.Common
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}