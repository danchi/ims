﻿using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.Employees.Commands.UpdateEmployeeCommand
{
    public class UpdateEmployeeCommandHandler : IRequestHandler<UpdateEmployeeCommand>
    {
        private readonly IIMSDbContext _context;
        private readonly IMediator _mediator;
        private readonly UpdateEmployeeCommandValidation _validation;

        public UpdateEmployeeCommandHandler(IIMSDbContext context, IMediator mediator,
            UpdateEmployeeCommandValidation validation)
        {
            _context = context;
            _mediator = mediator;
            _validation = validation;
        }

        public async Task<Unit> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            var existEmployee = await _context.Employees.FindAsync(request.Id);

            if (existEmployee == null)
                throw new NotFoundException(nameof(Employee), request.Id);

            existEmployee.IsActive = request.IsActive;
            existEmployee.UserName = request.UserName;

            await _context.SaveChangesAsync(cancellationToken);

            await _mediator.Publish(new EmployeeUpdated() { Id = request.Id }, cancellationToken);

            return Unit.Value;
        }
    }
}