﻿using MediatR;

namespace IMS.Application.Employees.Commands.UpdateEmployeeCommand
{
    public class UpdateEmployeeCommand : IRequest
    {
        public string Id { get; set; }
        public bool IsActive { get; set; }
        public string UserName { get; set; }
    }
}