﻿using MediatR;

namespace IMS.Application.Employees.Commands.UpdateEmployeeCommand
{
    public class EmployeeUpdated : INotification
    {
        public string Id { get; set; }
    }
}