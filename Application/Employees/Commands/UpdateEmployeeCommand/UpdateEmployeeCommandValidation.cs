﻿using FluentValidation;

namespace IMS.Application.Employees.Commands.UpdateEmployeeCommand
{
    public class UpdateEmployeeCommandValidation : AbstractValidator<Application.Employees.Commands.UpdateEmployeeCommand.UpdateEmployeeCommand>
    {
        public UpdateEmployeeCommandValidation()
        {
            RuleFor(s => s.Id)
                .NotNull()
                .NotEmpty();
            RuleFor(s => s.IsActive)
                .NotNull();
            RuleFor(s => s.UserName)
                .MinimumLength(10)
                .MaximumLength(50);

        }
    }
}