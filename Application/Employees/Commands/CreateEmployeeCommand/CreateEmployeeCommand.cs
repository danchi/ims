﻿using MediatR;

namespace IMS.Application.Employees.Commands.CreateEmployeeCommand
{
    public class CreateEmployeeCommand : IRequest
    {
        public string Id { get; set; }

        public string UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Company { get; set; }

        public bool IsActive { get; set; }
        public string UserName { get; set; }
    }
}