﻿using FluentValidation;

namespace IMS.Application.Employees.Commands.CreateEmployeeCommand
{
    public class CreateEmployeeCommandValidation : AbstractValidator<CreateEmployeeCommand>
    {
        public CreateEmployeeCommandValidation()
        {
            RuleFor(x => x.UserName)
                .MaximumLength(50)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.Company)
                .NotNull()
                .NotEmpty()
                .MaximumLength(50);
            RuleFor(x => x.UserId)
                .NotEmpty()
                .NotEmpty()
                .MaximumLength(450);
            RuleFor(x => x.IsActive)
                .NotNull();
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
            RuleFor(x => x.LastName)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
        }
    }
}