﻿using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.Employees.Commands.CreateEmployeeCommand
{
    public class CreateEmployeeCommandHandler : IRequestHandler<CreateEmployeeCommand>
    {
        private readonly IIMSDbContext _context;
        private readonly IMediator _mediator;
        private readonly CreateEmployeeCommandValidation _validation;


        public CreateEmployeeCommandHandler(IIMSDbContext context, IMediator mediator, CreateEmployeeCommandValidation validation)
        {
            _context = context;
            _mediator = mediator;
            _validation = validation;
        }

        public async Task<Unit> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {

            var validationResults = _validation.Validate(request);

            if (!validationResults.IsValid)
                throw new ValidationException(validationResults.Errors.ToList());

            var existEmployee = await _context.Employees.AnyAsync(x => x.UserName == request.UserName, cancellationToken);

            if (existEmployee)
                throw new AlreadyExistException(nameof(Employee), request.UserName);

            var entity = new Employee()
            {
                Id = Guid.NewGuid().ToString(),
                Company = request.Company,
                FirstName = request.FirstName,
                LastName = request.LastName,
                IsActive = request.IsActive,
                UserId = request.UserId,
                UserName = request.UserName
            };

            _context.Employees.Add(entity);
            await _context.SaveChangesAsync(cancellationToken);
            await _mediator.Publish(new EmployeeCreated() { UserName = request.UserName }, cancellationToken);

            return Unit.Value;
        }
    }
}