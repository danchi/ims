﻿using MediatR;

namespace IMS.Application.Employees.Commands.CreateEmployeeCommand
{
    public class EmployeeCreated : INotification
    {
        public string UserName { get; set; }
    }
}