﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Domain.Entities;

namespace IMS.Application.Employees.Queries.GetEmployeesListQuery
{
    public class EmployeeLookupDto : IMapFrom<Employee>
    {
        public string Id { get; set; }

        public string UserName { get; set; }

        public string Company { get; set; }

        public bool IsActive { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Employee, EmployeeLookupDto>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.UserName, opt => opt.MapFrom(s => s.UserName))
                .ForMember(d => d.Company, opt => opt.MapFrom(s => s.Company))
                .ForMember(d => d.IsActive, opt => opt.MapFrom(s => s.IsActive))
                .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.FirstName))
                .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.LastName));

        }
    }
}