﻿using System.Collections.Generic;

namespace IMS.Application.Employees.Queries.GetEmployeesListQuery
{
    public class EmployeesListVm
    {
        public IList<EmployeeLookupDto> Employees { get; set; }
    }
}