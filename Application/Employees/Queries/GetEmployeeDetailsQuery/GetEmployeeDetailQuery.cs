﻿using MediatR;

namespace IMS.Application.Employees.Queries.GetEmployeeDetailsQuery
{
    public class GetEmployeeDetailQuery : IRequest<EmployeeDetailVm>
    {
        public string Id { get; set; }
    }
}