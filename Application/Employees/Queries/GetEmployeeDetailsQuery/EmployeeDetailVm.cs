﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Domain.Entities;

namespace IMS.Application.Employees.Queries.GetEmployeeDetailsQuery
{
    public class EmployeeDetailVm : IMapFrom<Employee>
    {
        public string UserName { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<Employee, EmployeeDetailVm>()
                .ForMember(x => x.UserName, opt => opt.MapFrom(x => x.UserName));
        }
    }
}