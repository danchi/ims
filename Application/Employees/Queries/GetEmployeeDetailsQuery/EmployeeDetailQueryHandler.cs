﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;

namespace IMS.Application.Employees.Queries.GetEmployeeDetailsQuery
{
    public class EmployeeDetailQueryHandler : IRequestHandler<GetEmployeeDetailQuery, EmployeeDetailVm>
    {
        private readonly IIMSDbContext _context;
        private readonly IMapper _mapper;

        public EmployeeDetailQueryHandler(IIMSDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public async Task<EmployeeDetailVm> Handle(GetEmployeeDetailQuery request, CancellationToken cancellationToken)
        {
            var employee = await _context.Employees.FindAsync(request.Id);

            if (employee == null)
                throw new NotFoundException(nameof(Employee), request.Id);

            return _mapper.Map<EmployeeDetailVm>(employee);
        }
    }
}