﻿using IMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application
{
    public interface IIMSDbContext
    {
        DbSet<ItemType> ItemTypes { get; set; }
        DbSet<Item> Items { get; set; }
        DbSet<Employee> Employees { get; set; }
        DbSet<ItemDetail> ItemDetails { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}