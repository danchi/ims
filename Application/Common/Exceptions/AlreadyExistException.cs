﻿using System;

namespace IMS.Application.Common.Exceptions
{
    public class AlreadyExistException : Exception
    {
        public AlreadyExistException(string name, object key) : base($"Entity {name} already exist with name {key}")
        {

        }
    }
}