﻿using IMS.Application.Items.Queries.GetItemDetails;
using MediatR;

namespace IMS.Application.Items.Queries.GetItemInfo
{
    public class GetItemInfoQuery : IRequest<ItemInfoVm>
    {
        public int Id { get; set; }
    }
}