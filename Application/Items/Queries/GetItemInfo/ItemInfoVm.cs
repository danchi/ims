﻿using System;
using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Domain.Entities;

namespace IMS.Application.Items.Queries.GetItemInfo
{
    public class ItemInfoVm : IMapFrom<Item>
    {
        public int Id { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }
        //public string PersonInCharge { get; set; }
        //public DateTime? WarrantyExpires { get; set; }
        //public bool IsDisposed { get; set; }

        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        //public string ModifiedBy { get; set; }
        public DateTime? Modified { get; set; }

        public string ItemTypeName { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Item, ItemInfoVm>()
                .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id == Id))
                .ForMember(x => x.ItemTypeName, opt => opt.MapFrom(x => x.ItemType.Name))
                .ForMember(x => x.Created, opt => opt.MapFrom(x => x.Created))
                .ForMember(x => x.CreatedBy, opt => opt.MapFrom(x => x.CreatedBy))
                .ForMember(x => x.Modified, opt => opt.MapFrom(x => x.Modified))
                .ForMember(x => x.Description, opt => opt.MapFrom(x => x.Description));

        }
    }
}