﻿using AutoMapper;
using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using IMS.Application.Items.Queries.GetItemInfo;

namespace IMS.Application.Items.Queries.GetItemDetails
{
    public class GetItemInfoQueryHandler : IRequestHandler<GetItemInfoQuery, ItemInfoVm>
    {
        private readonly IIMSDbContext _context;
        private readonly IMapper _mapper;

        public GetItemInfoQueryHandler(IIMSDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ItemInfoVm> Handle(GetItemInfoQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Items.FindAsync(request.Id);

            if (entity == null)
                throw new NotFoundException(nameof(Item), request.Id);

            return _mapper.Map<ItemInfoVm>(entity);


        }
    }
}