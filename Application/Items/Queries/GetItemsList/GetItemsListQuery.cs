﻿using System.Collections.Generic;
using MediatR;

namespace IMS.Application.Items.Queries.GetItemsList
{
    public class GetItemsListQuery : IRequest<ItemListVm>
    {
        public IList<ItemListVm> Items { get; set; }
    }
}