﻿using System.Collections.Generic;

namespace IMS.Application.Items.Queries.GetItemsList
{
    public class ItemListVm
    {

        public IList<ItemLookupDto> Items { get; set; }
    }
}