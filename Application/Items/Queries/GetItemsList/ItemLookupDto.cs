﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Domain.Entities;
using System;

namespace IMS.Application.Items.Queries.GetItemsList
{
    public class ItemLookupDto : IMapFrom<Item>
    {
        public int Id { get; set; }

        public int ItemTypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Employee { get; set; }
        public DateTime? WarrantyExpires { get; set; }
        public bool IsDisposed { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? Modified { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Item, ItemLookupDto>()
                .ForMember(d => d.Employee, opt => opt.MapFrom(s => s.Employee.UserName));
        }
    }
}