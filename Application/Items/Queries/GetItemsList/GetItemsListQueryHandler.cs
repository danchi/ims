﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.Items.Queries.GetItemsList
{
    public class GetItemsListQueryHandler : IRequestHandler<GetItemsListQuery, ItemListVm>
    {
        private readonly IIMSDbContext _context;
        private readonly IMapper _mapper;

        public GetItemsListQueryHandler(IIMSDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ItemListVm> Handle(GetItemsListQuery request, CancellationToken cancellationToken)
        {
            var items = await _context.Items
                .ProjectTo<ItemLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);

            var vm = new ItemListVm
            {
                Items = items
            };

            return vm;
        }
    }
}