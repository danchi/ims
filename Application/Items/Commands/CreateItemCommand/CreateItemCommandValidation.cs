﻿using FluentValidation;

namespace IMS.Application.Items.Commands.CreateItemCommand
{
    public class CreateItemCommandValidation : AbstractValidator<Application.Items.Commands.CreateItemCommand.CreateItemCommand>
    {
        public CreateItemCommandValidation()
        {
            RuleFor(s => s.Code)
                .NotNull()
                .NotEmpty()
                .MaximumLength(50);
            RuleFor(s => s.Description)
                .MaximumLength(500);
            RuleFor(s => s.IsDisposed)
                .NotNull();
            RuleFor(s => s.Employee)
                .MaximumLength(50)
                .NotNull()
                .NotEmpty();
        }
    }
}