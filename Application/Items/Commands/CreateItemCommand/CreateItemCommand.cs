﻿using MediatR;
using System;

namespace IMS.Application.Items.Commands.CreateItemCommand
{
    public class CreateItemCommand : IRequest
    {
        public string Code { get; set; }

        public string Description { get; set; }
        public bool IsDisposed { get; set; }
        public string Employee { get; set; }
        public DateTime? WarrantyExpires { get; set; }
        public int ItemTypeId { get; set; }

    }
}