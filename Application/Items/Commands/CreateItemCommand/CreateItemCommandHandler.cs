﻿using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.Items.Commands.CreateItemCommand
{
    public class CreateItemCommandHandler : IRequestHandler<CreateItemCommand>
    {
        private readonly IIMSDbContext _context;
        private readonly IMediator _mediator;
        private readonly CreateItemCommandValidation _validation;

        public CreateItemCommandHandler(IIMSDbContext context, IMediator mediator,
            CreateItemCommandValidation validation)
        {
            _context = context;
            _mediator = mediator;
            _validation = validation;
        }

        public async Task<Unit> Handle(CreateItemCommand request, CancellationToken cancellationToken)
        {
            var existItem = _context.Items.Any(x => x.Code.ToUpper() == request.Code.ToUpper());

            if (existItem)
                throw new AlreadyExistException(nameof(Item), request.Code);

            var validation = _validation.Validate(request);

            if (!validation.IsValid)
                throw new ValidationException(validation.Errors.ToList());

            var entity = new Item
            {
                Code = request.Code.ToUpper(),
                Description = request.Description,
                EmployeeId = request.Employee,
                WarrantyExpires = request.WarrantyExpires,
                IsDisposed = request.IsDisposed,
                ItemTypeId = request.ItemTypeId
            };

            _context.Items.Add(entity);
            await _context.SaveChangesAsync(cancellationToken);
            await _mediator.Publish(new ItemCreated() { Code = entity.Code }, cancellationToken);
            return Unit.Value;
        }
    }
}