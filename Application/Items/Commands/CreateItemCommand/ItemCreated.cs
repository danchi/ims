﻿using MediatR;

namespace IMS.Application.Items.Commands.CreateItemCommand
{
    public class ItemCreated : INotification
    {
        public string Code { get; set; }
    }
}