﻿using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.Items.Commands.UpdateItemCommand
{
    public class UpdateItemCommandHandler : IRequestHandler<UpdateItemCommandRequest>
    {
        private readonly IIMSDbContext _context;
        private readonly IMediator _mediator;

        public UpdateItemCommandHandler(IIMSDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(UpdateItemCommandRequest request, CancellationToken cancellationToken)
        {
            var entity = await _context.Items.SingleOrDefaultAsync(w => w.Id == request.Id, cancellationToken: cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(Item), request.Id);

            entity.Description = request.Description;
            entity.IsDisposed = request.IsDisposed;
            entity.WarrantyExpires = request.WarrantyExpires;

            await _context.SaveChangesAsync(cancellationToken);

            await _mediator.Publish(new UpdatedItemCommand()
            {
                Id = entity.Id
            }, cancellationToken);
            return Unit.Value;
        }
    }
}