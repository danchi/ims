﻿using System;

namespace IMS.Application.Items.Commands.UpdateItemCommand
{
    public class UpdatedItemCommand
    {
        public DateTime Modified { get; set; }
        public int Id { get; set; }
    }
}