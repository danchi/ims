﻿using MediatR;
using System;

namespace IMS.Application.Items.Commands.UpdateItemCommand
{
    public class UpdateItemCommandRequest : IRequest
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public bool IsDisposed { get; set; }

        public DateTime? WarrantyExpires { get; set; }
    }
}