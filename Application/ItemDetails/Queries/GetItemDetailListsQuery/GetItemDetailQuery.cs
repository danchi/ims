﻿using MediatR;

namespace IMS.Application.ItemDetails.Queries.GetItemDetailListsQuery
{
    public class GetItemDetailQuery : IRequest<ItemDetailVm>
    {
        public int ItemId { get; set; }
    }
}