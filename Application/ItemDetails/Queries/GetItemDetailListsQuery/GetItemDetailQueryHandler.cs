﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.ItemDetails.Queries.GetItemDetailListsQuery
{
    public class GetItemDetailQueryHandler : IRequestHandler<GetItemDetailQuery, ItemDetailVm>
    {
        private readonly IIMSDbContext _context;
        private readonly IMapper _mapper;

        public GetItemDetailQueryHandler(IIMSDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ItemDetailVm> Handle(GetItemDetailQuery request, CancellationToken cancellationToken)
        {
            var itemDetails = await _context.ItemDetails
                .ProjectTo<ItemDetailLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken);
            var vm = new ItemDetailVm()
            {
                ItemDetails = itemDetails
            };

            return vm;
        }
    }
}