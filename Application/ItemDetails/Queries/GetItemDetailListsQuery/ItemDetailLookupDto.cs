﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Domain.Entities;
using System;

public class ItemDetailLookupDto : IMapFrom<ItemDetail>
{
    public int Id { get; set; }

    public string ItemName { get; set; }

    public string Key { get; set; }

    public string Value { get; set; }

    public string CreatedBy { get; set; }
    public DateTime Created { get; set; }

    public string ModifiedBy { get; set; }
    public DateTime? Modified { get; set; }
    public void Mapping(Profile profile)
    {
        profile.CreateMap<ItemDetail, ItemDetailLookupDto>()
            .ForMember(x => x.Id, opt => opt.MapFrom(x => x.Id))
            .ForMember(d => d.Created, opt => opt.MapFrom(s => s.Created))
            .ForMember(d => d.CreatedBy, opt => opt.MapFrom(s => s.CreatedBy))
            .ForMember(d => d.ItemName, opt => opt.MapFrom(s => s.Item.Code))
            .ForMember(d => d.Key, opt => opt.MapFrom(s => s.Key))
            .ForMember(d => d.Value, opt => opt.MapFrom(s => s.Value))
            .ForMember(d => d.Modified, opt => opt.MapFrom(s => s.Modified))
            .ForMember(d => d.ModifiedBy, opt => opt.MapFrom(s => s.ModifiedBy));
    }
}