﻿using System.Collections.Generic;

namespace IMS.Application.ItemDetails.Queries.GetItemDetailListsQuery
{
    public class ItemDetailVm
    {
        public IList<ItemDetailLookupDto> ItemDetails { get; set; }


    }
}