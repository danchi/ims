﻿using MediatR;

namespace IMS.Application.ItemDetails.Commands.UpdateItemDetailCommand
{
    public class UpdateItemDetailCommand : IRequest
    {
        public string Key { get; set; }

        public string Value { get; set; }
        public int Id { get; set; }
    }
}