﻿using MediatR;

namespace IMS.Application.ItemDetails.Commands.UpdateItemDetailCommand
{
    public class UpdatedItemDetail : INotification
    {
        public int Id { get; set; }
    }
}