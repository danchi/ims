﻿using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.ItemDetails.Commands.UpdateItemDetailCommand
{
    public class UpdateItemDetailCommandHandler : IRequestHandler<UpdateItemDetailCommand>
    {
        private readonly IIMSDbContext _context;
        private readonly IMediator _mediator;

        public UpdateItemDetailCommandHandler(IIMSDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }
        public async Task<Unit> Handle(UpdateItemDetailCommand request, CancellationToken cancellationToken)
        {
            var itemDetail = await _context.ItemDetails.FindAsync(request.Id);
            if (itemDetail == null)
                throw new NotFoundException(nameof(ItemDetail), request.Id);

            itemDetail.Key = request.Key;
            itemDetail.Value = request.Value;

            await _context.SaveChangesAsync(cancellationToken);

            await _mediator.Publish(new UpdatedItemDetail() { Id = request.Id },
                cancellationToken);

            return Unit.Value;
        }
    }
}