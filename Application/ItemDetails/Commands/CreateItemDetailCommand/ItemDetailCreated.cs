﻿using MediatR;

namespace IMS.Application.ItemDetails.Commands.CreateItemDetailCommand
{
    public class ItemDetailCreated : INotification
    {
        public int ItemId { get; set; }
    }
}