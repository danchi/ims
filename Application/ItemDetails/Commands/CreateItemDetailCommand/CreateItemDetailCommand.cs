﻿using MediatR;

namespace IMS.Application.ItemDetails.Commands.CreateItemDetailCommand
{
    public class CreateItemDetailCommand : IRequest
    {
        public int ItemId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }
}