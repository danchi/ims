﻿using FluentValidation;

namespace IMS.Application.ItemDetails.Commands.CreateItemDetailCommand
{
    public class CreateItemDetailCommandValidation : AbstractValidator<CreateItemDetailCommand>
    {
        public CreateItemDetailCommandValidation()
        {
            RuleFor(s => s.ItemId)
                .NotEmpty()
                .NotNull()
                .NotEqual(0);
            RuleFor(s => s.Key)
                .NotEmpty()
                .NotNull()
                .MaximumLength(255);
            RuleFor(s => s.Value)
                .NotNull()
                .NotEmpty()
                .MaximumLength(255);
        }
    }
}