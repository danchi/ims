﻿using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.ItemDetails.Commands.CreateItemDetailCommand
{
    public class CreateItemDetailCommandHandler : IRequestHandler<CreateItemDetailCommand>
    {
        private readonly IIMSDbContext _context;
        private readonly IMediator _mediator;
        private readonly CreateItemDetailCommandValidation _validation;

        public CreateItemDetailCommandHandler(IIMSDbContext context, IMediator mediator, CreateItemDetailCommandValidation validation)
        {
            _context = context;
            _mediator = mediator;
            _validation = validation;
        }

        public async Task<Unit> Handle(CreateItemDetailCommand request, CancellationToken cancellationToken)
        {

            var validationResults = _validation.Validate(request);

            if (!validationResults.IsValid)
                throw new ValidationException(validationResults.Errors.ToList());

            var existEntity = await _context.ItemDetails.AnyAsync(x => x.Key == request.Key, cancellationToken);

            if (existEntity)
                throw new AlreadyExistException(nameof(ItemDetail), request.Key);

            var newEntity = new ItemDetail()
            {
                ItemId = request.ItemId,
                Key = request.Key,
                Value = request.Value
            };

            _context.ItemDetails.Add(newEntity);
            await _context.SaveChangesAsync(cancellationToken);

            await _mediator.Publish(new ItemDetailCreated() { ItemId = request.ItemId }, cancellationToken);

            return Unit.Value;
        }
    }
}