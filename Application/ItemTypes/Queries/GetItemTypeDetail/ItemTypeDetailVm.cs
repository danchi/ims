﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Domain.Entities;

namespace IMS.Application.ItemTypes.Queries.GetItemTypeDetail
{
    public class ItemTypeDetailVm : IMapFrom<ItemType>
    {
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ItemType, ItemTypeDetailVm>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => s.Id));
        }

        public string Name { get; set; }

        public int Id { get; set; }
    }
}