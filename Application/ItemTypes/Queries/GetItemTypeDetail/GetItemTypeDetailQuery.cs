﻿using MediatR;

namespace IMS.Application.ItemTypes.Queries.GetItemTypeDetail
{
    public class GetItemTypeDetailQuery : IRequest<ItemTypeDetailVm>
    {
        public int Id { get; set; }
    }
}