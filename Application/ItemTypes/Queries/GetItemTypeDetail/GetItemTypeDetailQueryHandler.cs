﻿using AutoMapper;
using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.ItemTypes.Queries.GetItemTypeDetail
{
    public class GetItemTypeDetailQueryHandler : IRequestHandler<GetItemTypeDetailQuery, ItemTypeDetailVm>
    {
        private readonly IIMSDbContext _context;
        private readonly IMapper _mapper;

        public GetItemTypeDetailQueryHandler(IIMSDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ItemTypeDetailVm> Handle(GetItemTypeDetailQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.ItemTypes
                .FindAsync(request.Id);

            if (entity == null)
                throw new NotFoundException(nameof(ItemType), request.Id);

            return _mapper.Map<ItemTypeDetailVm>(entity);
        }
    }
}