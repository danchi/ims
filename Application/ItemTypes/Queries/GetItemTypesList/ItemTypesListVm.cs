﻿using System.Collections.Generic;

namespace IMS.Application.ItemTypes.Queries.GetItemTypesList
{
    public class ItemTypesListVm
    {
        public IList<ItemTypeLookupDto> ItemTypes { get; set; }
    }
}