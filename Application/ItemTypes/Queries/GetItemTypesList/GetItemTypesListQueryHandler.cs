﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.ItemTypes.Queries.GetItemTypesList
{
    public class GetItemTypesListQueryHandler : IRequestHandler<GetItemTypesListQuery, ItemTypesListVm>
    {
        private readonly IIMSDbContext _context;
        private readonly IMapper _mapper;

        public GetItemTypesListQueryHandler(IIMSDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ItemTypesListVm> Handle(GetItemTypesListQuery request, CancellationToken cancellationToken)
        {
            var itemTypes = await _context
                .ItemTypes
                .ProjectTo<ItemTypeLookupDto>(_mapper.ConfigurationProvider)
                .ToListAsync(cancellationToken: cancellationToken);

            var vm = new ItemTypesListVm
            {
                ItemTypes = itemTypes
            };
            return vm;
        }
    }
}