﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Domain.Entities;

namespace IMS.Application.ItemTypes.Queries.GetItemTypesList
{
    public class ItemTypeLookupDto : IMapFrom<ItemType>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<ItemType, ItemTypeLookupDto>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(x => x.Name, opt => opt.MapFrom(s => s.Name));
        }
    }
}