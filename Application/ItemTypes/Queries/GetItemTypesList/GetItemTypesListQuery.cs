﻿using MediatR;

namespace IMS.Application.ItemTypes.Queries.GetItemTypesList
{
    public class GetItemTypesListQuery : IRequest<ItemTypesListVm>
    {
    }
}