﻿using MediatR;

namespace IMS.Application.ItemTypes.Commands.CreateItemType
{
    public class CreateItemTypeCommand : IRequest
    {

        public string Name { get; set; }
    }
}