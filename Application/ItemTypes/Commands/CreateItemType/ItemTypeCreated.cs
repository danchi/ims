﻿using MediatR;

namespace IMS.Application.ItemTypes.Commands.CreateItemType
{
    public class ItemTypeCreated : INotification
    {
        public string Name { get; set; }
    }
}