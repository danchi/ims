﻿using FluentValidation;

namespace IMS.Application.ItemTypes.Commands.CreateItemType
{
    public class CreateItemTypeCommandValidator : AbstractValidator<CreateItemTypeCommand>
    {
        public CreateItemTypeCommandValidator()
        {
            RuleFor(x => x.Name)
                .MaximumLength(150)
                .NotEmpty()
                .NotNull();
        }
    }
}