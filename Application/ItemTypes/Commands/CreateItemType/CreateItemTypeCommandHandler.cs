﻿using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.ItemTypes.Commands.CreateItemType
{
    public class CreateItemTypeCommandHandler : IRequestHandler<CreateItemTypeCommand>
    {
        private readonly IIMSDbContext _context;
        private readonly IMediator _mediator;


        public CreateItemTypeCommandHandler(IIMSDbContext context, IMediator mediator)
        {
            _context = context;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(CreateItemTypeCommand request, CancellationToken cancellationToken)
        {
            var existItemType = _context.ItemTypes.Any(x => x.Name == request.Name);
            if (existItemType)
                throw new AlreadyExistException(nameof(ItemType), request.Name);



            var entity = new ItemType
            {
                Name = request.Name
            };

            _context.ItemTypes.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            await _mediator.Publish(new ItemTypeCreated() { Name = entity.Name }, cancellationToken);

            return Unit.Value;
        }
    }
}