﻿using MediatR;

namespace IMS.Application.ItemTypes.Commands.DeleteItemType
{
    public class DeleteItemTypeCommand : IRequest
    {
        public int Id { get; set; }
    }
}