﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IMS.Application.Common.Exceptions;
using IMS.Domain.Entities;
using MediatR;

namespace IMS.Application.ItemTypes.Commands.DeleteItemType
{
    public class DeleteItemTypeCommandHandler : IRequestHandler<IMS.Application.ItemTypes.Commands.DeleteItemType.DeleteItemTypeCommand>
    {
        private readonly IIMSDbContext _context;

        public DeleteItemTypeCommandHandler(IIMSDbContext context)
        {
            _context = context;
        }
        public async Task<Unit> Handle(IMS.Application.ItemTypes.Commands.DeleteItemType.DeleteItemTypeCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.ItemTypes.FindAsync(request.Id);

            if (entity == null)
                throw new NotFoundException(nameof(ItemType), request.Id);

            var hasItems = _context.Items.Any(i => i.ItemTypeId == entity.Id);

            if (hasItems)
                throw new DeleteFailureException(nameof(ItemType), request.Id, "There are existing items associated with this item type");


            _context.ItemTypes.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}