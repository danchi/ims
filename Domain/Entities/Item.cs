﻿using IMS.Domain.Common;
using System;

namespace IMS.Domain.Entities
{
    public class Item : AuditableEntity
    {
        public int Id { get; set; }

        public int ItemTypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string EmployeeId { get; set; }
        public DateTime? WarrantyExpires { get; set; }
        public bool IsDisposed { get; set; }

        public ItemType ItemType { get; set; }

        public Employee Employee { get; set; }

    }
}