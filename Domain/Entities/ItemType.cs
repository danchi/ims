﻿using System.Collections.Generic;

namespace IMS.Domain.Entities
{
    public class ItemType
    {
        public ItemType()
        {
            Items = new HashSet<Item>();
        }


        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Item> Items { get; set; }
    }
}