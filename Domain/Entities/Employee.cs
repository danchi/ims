﻿using System.Collections.Generic;

namespace IMS.Domain.Entities
{
    public class Employee
    {
        public Employee()
        {
            Items = new HashSet<Item>();
        }
        public string Id { get; set; }

        public string UserName { get; set; }

        public string UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Company { get; set; }

        public bool IsActive { get; set; }

        public ICollection<Item> Items { get; set; }
    }
}
