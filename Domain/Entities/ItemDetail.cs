﻿using IMS.Domain.Common;

namespace IMS.Domain.Entities
{
    public class ItemDetail : AuditableEntity
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public Item Item { get; set; }
    }
}