﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Application.ItemDetails.Queries.GetItemDetailListsQuery;
using IMS.Application.UnitTests.Common;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.ItemDetails.Queries.GetItemDetailListsQuery
{
    public class GetItemDetailListsQueryHandlerTests : CommandTestDatabase
    {
        private readonly IMapper _mapper;
        public GetItemDetailListsQueryHandlerTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()));
        }

        [Test]
        [TestCase(1)]
        public async Task Handle_GetItemDetails(int itemId)
        {
            var sut = new GetItemDetailQueryHandler(Context, _mapper);

            var result = await sut.Handle(new GetItemDetailQuery()
            {
                ItemId = itemId
            }, CancellationToken.None);

            Assert.AreEqual(1, result.ItemDetails.Count);
        }
    }
}