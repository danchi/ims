﻿using IMS.Application.ItemDetails.Commands.CreateItemDetailCommand;
using IMS.Application.UnitTests.Common;
using MediatR;
using Moq;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.ItemDetails.Commands.CreateItemDetailCommand
{

    public class CreateItemDetailCommandHandlerTests : CommandTestDatabase
    {
        private readonly Mock<IMediator> _mediatorMock;
        private readonly CreateItemDetailCommandValidation _mockValidation;
        public CreateItemDetailCommandHandlerTests()
        {
            _mediatorMock = new Mock<IMediator>();
            _mockValidation = new CreateItemDetailCommandValidation();
        }

        [Test]
        [TestCase(1, "Product Number", "some serial number of device")]
        public async Task Handle_CreateItemDetails(int itemId, string key, string value)
        {

            var sut = new CreateItemDetailCommandHandler(Auditable, _mediatorMock.Object, _mockValidation);

            var response = await sut.Handle(new Application.ItemDetails.Commands.CreateItemDetailCommand.CreateItemDetailCommand()
            {
                ItemId = itemId,
                Key = key,
                Value = value
            }, CancellationToken.None);

            _mediatorMock.Verify(
                p => p.Publish(It.Is<ItemDetailCreated>(m => m.ItemId == itemId), It.IsAny<CancellationToken>()),
                Times.Once);

        }

        [Test]
        [TestCase(0)]
        public void Handle_NotValidRequest(int itemId)
        {
            var sut = new CreateItemDetailCommandHandler(Auditable, _mediatorMock.Object, _mockValidation);
            var validationResult = _mockValidation.Validate(
                new Application.ItemDetails.Commands.CreateItemDetailCommand.CreateItemDetailCommand()
                { ItemId = itemId });
            Assert.False(validationResult.IsValid);

        }
    }
}