﻿using IMS.Application.Common.Exceptions;
using IMS.Application.ItemDetails.Commands.UpdateItemDetailCommand;
using IMS.Application.UnitTests.Common;
using MediatR;
using Moq;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.ItemDetails.Commands.UpdateItemDetailCommand
{
    public class UpdateItemDetailCommandHandlerTests : CommandTestDatabase
    {
        [Test]
        [TestCase(1, "updated key name", "updated value")]
        public async Task Handle_ValidItemDetail(int id, string key, string value)
        {
            var mediatorMock = new Mock<IMediator>();

            var sut = new UpdateItemDetailCommandHandler(Auditable, mediatorMock.Object);

            var response = await sut.Handle(new Application.ItemDetails.Commands.UpdateItemDetailCommand.UpdateItemDetailCommand()
            {
                Id = id,
                Key = key,
                Value = value
            }, CancellationToken.None);

            mediatorMock.Verify(m => m.Publish(It.Is<UpdatedItemDetail>(x => x.Id == id), It.IsAny<CancellationToken>()), Times.Once);
        }

        [Test]
        [TestCase(10)]
        public void Handle_NotValidItemDetailId(int id)
        {
            var mediator = new Mock<IMediator>();
            var sut = new UpdateItemDetailCommandHandler(Auditable, mediator.Object);

            Assert.ThrowsAsync<NotFoundException>(() => sut.Handle(
                  new Application.ItemDetails.Commands.UpdateItemDetailCommand.UpdateItemDetailCommand()
                  {
                      Id = id
                  }, CancellationToken.None));
        }
    }
}