﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IConfigurationProvider = AutoMapper.IConfigurationProvider;

namespace IMS.Application.UnitTests.Mappings
{
    public class MappingTestsFixture
    {
        public MappingTestsFixture()
        {
            ConfigurationProvider = new MapperConfiguration(cfg => { cfg.AddProfile<MappingProfile>(); });
            Mapper = ConfigurationProvider.CreateMapper();
        }

        public IConfigurationProvider ConfigurationProvider { get; }
        public IMapper Mapper { get; }
    }
}