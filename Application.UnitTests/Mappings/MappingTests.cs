﻿using AutoMapper;
using IMS.Application.Employees.Queries.GetEmployeeDetailsQuery;
using IMS.Application.Employees.Queries.GetEmployeesListQuery;
using IMS.Application.Items.Queries.GetItemInfo;
using IMS.Application.Items.Queries.GetItemsList;
using IMS.Application.ItemTypes.Queries.GetItemTypesList;
using IMS.Domain.Entities;
using Moq;
using NUnit.Framework;

namespace IMS.Application.UnitTests.Mappings
{
    public class MappingTests : MappingTestsFixture
    {
        private readonly Mock<IMapper> _mapperMock;
        public MappingTests()
        {
            _mapperMock = new Mock<IMapper>();
        }

        [Test]
        public void ShouldHaveValidConfiguration()
        {
            ConfigurationProvider.AssertConfigurationIsValid();
        }

        [Test]
        public void ShouldMapItemTypeToItemTypeDto()
        {

            var entity = new ItemType();
            var result = _mapperMock.Setup(s => s.Map<ItemTypeLookupDto>(entity));
            Assert.IsNotNull(result);
        }

        [Test]
        public void ShouldMapItemTypeToItemTypeDetailVm()
        {


            var entity = new ItemType();
            var result = _mapperMock.Setup(s => s.Map<ItemInfoVm>(entity));
            Assert.IsNotNull(result);
        }

        [Test]
        public void ShouldMapItemToItemDto()
        {

            var entity = new Item();
            var result = _mapperMock.Setup(s => s.Map<ItemLookupDto>(entity));
            Assert.IsNotNull(result);
        }
        [Test]
        public void ShouldMapItemToItemDetailVm()
        {

            var entity = new Item();
            var result = _mapperMock.Setup(s => s.Map<ItemInfoVm>(entity));
            Assert.IsNotNull(result);
        }

        [Test]
        public void ShouldMapEmployeeToEmployeeDto()
        {

            var entity = new Employee();
            var result = _mapperMock.Setup(s => s.Map<EmployeeLookupDto>(entity));
            Assert.IsNotNull(result);
        }

        [Test]
        public void ShouldMapEmployeeToEmployeeDetailVm()
        {
            var entity = new Employee();
            var result = _mapperMock.Setup(s => s.Map<EmployeeDetailVm>(entity));
            Assert.IsNotNull(result);
        }
    }
}