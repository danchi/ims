﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Application.Items.Queries.GetItemsList;
using IMS.Application.UnitTests.Common;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Items.Queries.GetItemsQueriesTests
{
    public class GetItemsQueriesHandlerTests : CommandTestDatabase
    {
        protected readonly IMapper Mapper;
        public GetItemsQueriesHandlerTests()
        {
            Mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()
            ));
        }

        [Test]
        public async Task Handle_GetItemsListQuery()
        {
            var sut = new GetItemsListQueryHandler(Auditable, Mapper);

            var response = await sut.Handle(new GetItemsListQuery(), CancellationToken.None);

            Assert.AreEqual(1, response.Items.Count);
        }
    }
}