﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Application.Items.Queries.GetItemDetails;
using IMS.Application.UnitTests.Common;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Items.Queries.GetItemInfoQuery
{
    public class GetItemDetailsQueryHandlerTests : CommandTestDatabase
    {
        private readonly IMapper _mapper;

        public GetItemDetailsQueryHandlerTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()
            ));
        }

        [Test]
        [TestCase(1)]
        public async Task GetItemInfo(int id)
        {
            var sut = new GetItemInfoQueryHandler(Context, _mapper);

            var result = await sut.Handle(new Application.Items.Queries.GetItemInfo.GetItemInfoQuery() { Id = id },
                CancellationToken.None);

            Assert.AreEqual("CODE001", result.Code);

        }
    }
}