﻿using IMS.Application.Common.Exceptions;
using IMS.Application.Items.Commands.CreateItemCommand;
using IMS.Application.UnitTests.Common;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Items.Commands.CreateItemCommand
{
    public class CreateItemCommandHandlerTests : CommandTestDatabase
    {


        [Test]
        [TestCase("CODE0001", "Computer for laser machine", false, "Danijel", "2019-10-17 10:55:25", 1)]
        public async Task Handle_ValidItem(string code, string description, bool isDisposed,
            string personInChargeId, DateTime? warrantyExpires, int itemTypeId)
        {
            var mediatorMock = new Mock<IMediator>();

            var mockValidation = new CreateItemCommandValidation();

            var sut = new CreateItemCommandHandler(Auditable, mediatorMock.Object, mockValidation);

            var response = await sut.Handle(new Application.Items.Commands.CreateItemCommand.CreateItemCommand
            {
                Code = code,
                Description = description,
                IsDisposed = isDisposed,
                Employee = personInChargeId,
                WarrantyExpires = warrantyExpires,
                ItemTypeId = itemTypeId
            }, CancellationToken.None);

            mediatorMock.Verify(m => m.Publish(It.Is<ItemCreated>(i => i.Code.ToUpper() == code.ToUpper()), CancellationToken.None));
        }
        [Test]
        [TestCase("CODE001", "Computer for laser machine", false, "Danijel", "2020-10-25", 1)]
        public void Handle_ValidItem_ThrowsAlreadyExistException(string code, string description, bool isDisposed,
       string userName, DateTime? warrantyExpires, int itemTypeId)
        {
            var mediatorMock = new Mock<IMediator>();
            var mockValidation = new CreateItemCommandValidation();
            var sut = new CreateItemCommandHandler(Auditable, mediatorMock.Object, mockValidation);

            Assert.ThrowsAsync<AlreadyExistException>(() =>
                sut.Handle(new Application.Items.Commands.CreateItemCommand.CreateItemCommand()
                {
                    Code = code,
                    Description = description,
                    IsDisposed = isDisposed,
                    Employee = userName,
                    WarrantyExpires = warrantyExpires,
                    ItemTypeId = itemTypeId
                }, CancellationToken.None));

        }



    }
}