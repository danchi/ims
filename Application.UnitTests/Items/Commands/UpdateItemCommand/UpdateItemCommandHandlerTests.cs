﻿using IMS.Application.Common.Exceptions;
using IMS.Application.Items.Commands.UpdateItemCommand;
using IMS.Application.UnitTests.Common;
using MediatR;
using Moq;
using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Items.Commands.UpdateItemCommand
{
    public class UpdateItemCommandHandlerTests : CommandTestDatabase
    {
        [Test]
        [TestCase(1, "new description", false, null)]
        public async Task Handle_UpdateValidItem(int id, string description, bool isDisposed, DateTime? warrantyExpires)
        {
            var mediatorMock = new Mock<IMediator>();

            var sut = new UpdateItemCommandHandler(Auditable, mediatorMock.Object);

            var response = await sut.Handle(new UpdateItemCommandRequest()
            {
                Id = id,
                Description = description,
                IsDisposed = isDisposed,
                WarrantyExpires = warrantyExpires
            }, CancellationToken.None);

            mediatorMock.Verify(m => m.Publish(It.Is<UpdatedItemCommand>(i => i.Id == id), It.IsAny<CancellationToken>()), Times.Once);
        }

        [Test]
        [TestCase(2)]
        public void Handle_UpdateItem_ThrowNotFoundException(int id)
        {
            var mediatorMock = new Mock<IMediator>();
            var sut = new UpdateItemCommandHandler(Auditable, mediatorMock.Object);

            Assert.ThrowsAsync<NotFoundException>(() => sut.Handle(new UpdateItemCommandRequest()
            {
                Id = id
            }, CancellationToken.None));
        }
    }
}