﻿using IMS.Persistence;
using System;

namespace IMS.Application.UnitTests.Common
{
    public class CommandTestDatabase : IDisposable
    {
        protected readonly IMSDbContext Context;
        protected readonly IMSDbContext Auditable;
        public CommandTestDatabase()
        {
            Context = IMSContextFactory.Create();
            Auditable = IMSContextFactory.AuditableCreation();

        }
        public void Dispose()
        {
            IMSContextFactory.Destroy(Context);
            IMSContextFactory.Destroy(Auditable);
        }
    }
}