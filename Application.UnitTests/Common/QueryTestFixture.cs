﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Persistence;
using System;

namespace IMS.Application.UnitTests.Common
{
    public class QueryTestFixture : IDisposable
    {
        public IMSDbContext Context { get; private set; }
        public IMapper Mapper { get; private set; }

        public QueryTestFixture()
        {
            Context = IMSContextFactory.Create();

            var configurationProvider = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            Mapper = configurationProvider.CreateMapper();
        }

        public void Dispose()
        {
            IMSContextFactory.Destroy(Context);
        }
    }
}