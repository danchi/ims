﻿using IMS.Application.Common.Interfaces;
using IMS.Common;
using IMS.Domain.Entities;
using IMS.Persistence;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;

namespace IMS.Application.UnitTests.Common
{
    public class IMSContextFactory
    {


        public static IMSDbContext AuditableCreation()
        {
            var options = new DbContextOptionsBuilder<IMSDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var currentUserServiceMock = new Mock<ICurrentUserService>();
            currentUserServiceMock.Setup(s => s.UserId).Returns("00000000-0000-0000-0000-000000000000");
            var dateTimeMock = new Mock<IDateTime>();
            dateTimeMock.Setup(s => s.Now).Returns(DateTime.Now);

            var context = new IMSDbContext(options, currentUserServiceMock.Object, dateTimeMock.Object);
            PopulateDatabase(context);

            return context;
        }

        public static IMSDbContext Create()
        {
            var options = new DbContextOptionsBuilder<IMSDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new IMSDbContext(options);

            PopulateDatabase(context);

            return context;
        }

        private static void PopulateDatabase(IMSDbContext context)
        {
            context.ItemTypes.AddRange(new[]
            {
                new ItemType()
                {
                    Id =1,
                    Name = "Fixed Assets"
                },
                new ItemType()
                {
                    Id = 2,
                    Name = "Spare"
                },
            });


            context.Items.Add(new Item()
            {
                Id = 1,
                ItemTypeId = 1,
                Code = "CODE001",
                Description = "Computer for laser machine",
                CreatedBy = "Danijel",
                Created = DateTime.Now,
                IsDisposed = false,
                EmployeeId = "Dario Terzic",
                WarrantyExpires = new DateTime(2020, 12, 25)
            });

            context.Employees.Add(new Employee()
            {
                Id = "00000000-0000-0000-0000-000000000000",
                UserName = "danijelb",
                Company = "Fiorano",
                UserId = Guid.NewGuid().ToString(),
                LastName = "Boksan",
                FirstName = "Danijel",
                IsActive = true
            });

            context.ItemDetails.Add(new ItemDetail()
            {
                Id = 1,
                ItemId = 1,
                Key = "Serial Number",
                Value = "4588552254"
            });

            context.SaveChanges();
            context.Database.EnsureCreated();
        }

        public static void Destroy(IMSDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}