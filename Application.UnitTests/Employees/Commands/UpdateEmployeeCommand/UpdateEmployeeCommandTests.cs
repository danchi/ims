﻿using IMS.Application.Common.Exceptions;
using IMS.Application.Employees.Commands.UpdateEmployeeCommand;
using IMS.Application.UnitTests.Common;
using MediatR;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Employees.Commands.UpdateEmployeeCommand
{
    public class UpdateEmployeeCommandTests : CommandTestDatabase
    {
        private readonly Mock<IMediator> _mediatorMock;
        public UpdateEmployeeCommandTests()
        {
            _mediatorMock = new Mock<IMediator>();
        }

        [Test]
        [TestCase("02000000-0000-0000-0000-000000000000")]
        public void Handle_NoValidId_ThrowNotFoundException(string id)
        {
            var validation = new UpdateEmployeeCommandValidation();

            var sut = new UpdateEmployeeCommandHandler(Context, _mediatorMock.Object, validation);

            Assert.ThrowsAsync<NotFoundException>(() =>
                sut.Handle(new Application.Employees.Commands.UpdateEmployeeCommand.UpdateEmployeeCommand() { Id = id }, CancellationToken.None));
        }

        [Test]
        [TestCase("00000000-0000-0000-0000-000000000000", false, "newUsername")]
        public async Task Handle_ValidEmployeeUpdate_ShouldUpdateEmployee(string id, bool isActive, string userName)
        {
            var validation = new UpdateEmployeeCommandValidation();

            var sut = new UpdateEmployeeCommandHandler(Context, _mediatorMock.Object, validation);

            var response = await sut.Handle(new Application.Employees.Commands.UpdateEmployeeCommand.UpdateEmployeeCommand()
            {
                Id = id,
                IsActive = isActive,
                UserName = userName
            }, CancellationToken.None);

            _mediatorMock.Verify(v => v.Publish(It.Is<EmployeeUpdated>(e => e.Id == id), It.IsAny<CancellationToken>()), Times.Once);
        }

    }
}