﻿using IMS.Application.Common.Exceptions;
using IMS.Application.Employees.Commands.CreateEmployeeCommand;
using IMS.Application.UnitTests.Common;
using MediatR;
using Moq;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Employees.Commands.CreateEmployeeCommand
{
    public class CreateEmployeeCommandTests : CommandTestDatabase
    {
        private readonly Mock<IMediator> _mediator;
        public CreateEmployeeCommandTests()
        {
            _mediator = new Mock<IMediator>();
        }

        [Test]
        [TestCase("boksand", "Danijel", "Boksan", "Fiorano", true, "456sdada-asda65da")]
        public async Task Handle_GivenValidEmployee_ReturnUserName(string userName, string firstName, string lastName, string company, bool isActive, string userId)
        {

            var empCommand = CreateEmployeeCommand(userName, firstName, lastName, company, isActive, userId);

            var mockValidation = new CreateEmployeeCommandValidation();

            var sut = new CreateEmployeeCommandHandler(Context, _mediator.Object, mockValidation);

            var response = await sut.Handle(empCommand, CancellationToken.None);

            _mediator.Verify(s => s.Publish(It.Is<EmployeeCreated>(e => e.UserName == userName), It.IsAny<CancellationToken>()), Times.Once);
        }

        [Test]
        [TestCase("", "Danijel", "Boksan", "Fiorano", true, "456sdada-asda65da")]
        [TestCase("danijelb", "Danijel", "Boksan", "", true, "456sdada-asda65da")]
        [TestCase("danijelb", "Danijel", "Boksan", "Fiorano", true, "")]
        public void Handle_NotValidUserNameEmployee_NotValidValidation(string userName, string firstName, string lastName, string company, bool isActive, string userId)
        {

            var empCommand = CreateEmployeeCommand(userName, firstName, lastName, company, isActive, userId);

            var mockValidation = new CreateEmployeeCommandValidation();
            var validationResult = mockValidation.Validate(empCommand);

            Assert.False(validationResult.IsValid);

        }

        [Test]
        [TestCase("danijelb", "Danijel", "Boksan", "Fiorano", true, "456sdada-asda65da")]
        public void Handle_GivenValidEmployee_ThrowAlreadyExistException(string userName, string firstName, string lastName, string company, bool isActive, string userId)
        {
            var empCommand = CreateEmployeeCommand(userName, firstName, lastName, company, isActive, userId);

            var mockValidation = new CreateEmployeeCommandValidation();

            var sut = new CreateEmployeeCommandHandler(Context, _mediator.Object, mockValidation);

            Assert.ThrowsAsync<AlreadyExistException>(() => sut.Handle(empCommand, CancellationToken.None));
        }

        private static Application.Employees.Commands.CreateEmployeeCommand.CreateEmployeeCommand CreateEmployeeCommand(string userName, string firstName, string lastName,
            string company, bool isActive, string userId)
        {
            var empCommand = new Application.Employees.Commands.CreateEmployeeCommand.CreateEmployeeCommand
            {
                Company = company,
                FirstName = firstName,
                LastName = lastName,
                IsActive = isActive,
                UserId = userId,
                UserName = userName
            };
            return empCommand;
        }
    }
}