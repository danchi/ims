﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Application.Employees.Queries.GetEmployeesListQuery;
using IMS.Application.UnitTests.Common;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Employees.Queries.GetEmployeesListQuery
{
    public class GetEmployeesListQueryTests : CommandTestDatabase
    {
        private readonly IMapper _mapper;
        private readonly GetEmployeesListQueryHandler _getEmployeeQueryHandler;
        public GetEmployeesListQueryTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()
            ));
            _getEmployeeQueryHandler = new GetEmployeesListQueryHandler(Auditable, _mapper);
        }

        [Test]
        public async Task Handle_EmployeesListQuery()
        {
            var response = await _getEmployeeQueryHandler.Handle(new Application.Employees.Queries.GetEmployeesListQuery.GetEmployeesListQuery(), CancellationToken.None);

            Assert.AreEqual(1, response.Employees.Count);
        }
    }
}