﻿using AutoMapper;
using IMS.Application.Common.Exceptions;
using IMS.Application.Common.Mapping;
using IMS.Application.Employees.Queries.GetEmployeeDetailsQuery;
using IMS.Application.UnitTests.Common;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.Employees.Queries.GetEmployeeDetailsQuery
{
    public class GetEmployeeDetailQueryTests : CommandTestDatabase
    {
        private readonly IMapper _mapper;

        public GetEmployeeDetailQueryTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()
            ));
        }

        [Test]
        [TestCase("00000000-0000-0000-0000-000000000000")]
        public async Task Handle_EmployeeValidId(string id)
        {
            var sut = new EmployeeDetailQueryHandler(Auditable, _mapper);

            var response = await sut.Handle(new GetEmployeeDetailQuery() { Id = id }, CancellationToken.None);

            Assert.AreEqual("danijelb", response.UserName);
        }

        [Test]
        [TestCase("00000000-0000-0000-0000-000000000100")]
        public void Handle_NotValidId_ThrowsNotFoundExceptions(string id)
        {
            var sut = new EmployeeDetailQueryHandler(Auditable, _mapper);

            Assert.ThrowsAsync<NotFoundException>(async () => await sut.Handle(new GetEmployeeDetailQuery() { Id = id },
               CancellationToken.None));
        }
    }
}