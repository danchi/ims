﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Application.ItemTypes.Queries.GetItemTypeDetail;
using IMS.Application.UnitTests.Common;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.ItemTypes.Queries
{
    public class GetItemTypeDetailQueryHandlerTests : CommandTestDatabase
    {
        private readonly IMapper _mapper;
        public GetItemTypeDetailQueryHandlerTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()
            ));
        }
        [Test]
        [TestCase(1)]
        public async Task GetItemTypeDetail(int id)
        {
            var sut = new GetItemTypeDetailQueryHandler(Context, _mapper);

            var result = await sut.Handle(new GetItemTypeDetailQuery() { Id = id }, CancellationToken.None);

            Assert.AreEqual("Fixed Assets", result.Name);

        }
    }
}
