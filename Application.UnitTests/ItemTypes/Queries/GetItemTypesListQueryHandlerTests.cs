﻿using AutoMapper;
using IMS.Application.Common.Mapping;
using IMS.Application.ItemTypes.Queries.GetItemTypesList;
using IMS.Application.UnitTests.Common;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;

namespace IMS.Application.UnitTests.ItemTypes.Queries
{
    public class GetItemTypesListQueryHandlerTests : CommandTestDatabase
    {
        private readonly IMapper _mapper;
        public GetItemTypesListQueryHandlerTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>()
                ));
        }
        [Test]
        public async Task GetItemTypesList()
        {

            var sut = new GetItemTypesListQueryHandler(Context, _mapper);
            var response = await sut.Handle(new GetItemTypesListQuery(), CancellationToken.None);
            Assert.AreEqual(2, response.ItemTypes.Count);

        }
    }
}