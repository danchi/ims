﻿using IMS.Application.Common.Exceptions;
using IMS.Application.ItemTypes.Commands.CreateItemType;
using IMS.Application.UnitTests.Common;
using MediatR;
using Moq;
using NUnit.Framework;
using System.Threading;

namespace IMS.Application.UnitTests.ItemTypes.Commands.CreateItemTypeCommand
{
    public class CreateItemTypeCommandTests : CommandTestDatabase
    {

        [Test]
        [TestCase("Consumable")]
        public void Handle_GiveValidItemTypeName(string name)
        {
            var mediatorMock = new Mock<IMediator>();
            var sut = new CreateItemTypeCommandHandler(Context, mediatorMock.Object);

            var response = sut.Handle(new Application.ItemTypes.Commands.CreateItemType.CreateItemTypeCommand { Name = name }, CancellationToken.None);

            mediatorMock.Verify(m => m.Publish(It.Is<ItemTypeCreated>(itc => itc.Name == name), It.IsAny<CancellationToken>()), Times.Once);

        }


        [Test]
        [TestCase("Fixed Assets")]
        public void Handle_GivenExistItemTypeName_ThrowsAlreadyExistException(string name)
        {
            var mediatorMock = new Mock<IMediator>();
            var sut = new CreateItemTypeCommandHandler(Context, mediatorMock.Object);

            Assert.ThrowsAsync<AlreadyExistException>(() => sut.Handle(new IMS.Application.ItemTypes.Commands.CreateItemType.CreateItemTypeCommand() { Name = name }, CancellationToken.None));
        }
    }
}