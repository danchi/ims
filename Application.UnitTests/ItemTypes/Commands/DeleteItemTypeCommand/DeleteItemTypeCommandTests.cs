﻿using IMS.Application.Common.Exceptions;
using IMS.Application.ItemTypes.Commands.DeleteItemType;
using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;
using IMS.Application.UnitTests.Common;

namespace IMS.Application.UnitTests.ItemTypes.Commands.DeleteItemTypeCommand
{

    public class DeleteItemTypeCommandTests : CommandTestDatabase
    {
        private readonly DeleteItemTypeCommandHandler _sut;
        public DeleteItemTypeCommandTests() : base()
        {
            _sut = new DeleteItemTypeCommandHandler(Context);
        }
        [Test]
        [TestCase(10)]
        public void Handle_GivenInvalidId_ThrowsNotFoundException(int id)
        {
            var deleteItemTypeCommand = new IMS.Application.ItemTypes.Commands.DeleteItemType.DeleteItemTypeCommand { Id = id };

            Assert.ThrowsAsync<NotFoundException>(
                () => _sut.Handle(deleteItemTypeCommand, CancellationToken.None));
        }

        [Test]
        [TestCase(2)]
        public async Task Handle_GivenValidIdAndZeroItems_DeleteItemType(int id)
        {
            var command = new IMS.Application.ItemTypes.Commands.DeleteItemType.DeleteItemTypeCommand { Id = id };

            await _sut.Handle(command, CancellationToken.None);
            var itemType = await Context.ItemTypes.FindAsync(id);
            Assert.Null(itemType);
        }

        [Test]
        [TestCase(1)]
        public void Handle_GivenValidIdAndSomeItems_ThrowsFailureException(int id)
        {
            var command = new IMS.Application.ItemTypes.Commands.DeleteItemType.DeleteItemTypeCommand() { Id = id };

            Assert.ThrowsAsync<DeleteFailureException>(() => _sut.Handle(command, CancellationToken.None));
        }
    }
}